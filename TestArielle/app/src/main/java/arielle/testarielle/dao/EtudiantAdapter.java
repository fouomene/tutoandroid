package arielle.testarielle.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by FOUOMENE on 20/06/2015.
 */
public class EtudiantAdapter {



    private static final int BASE_VERSION = 12;
    private static final String BASE_NOM = "etudiant.db";
    private static final String TABLE_NAME_etudiant = "etudiant";
    private static final String TABLE_NAME_note = "note";

    private static final String COLONNE_ID_etudiant = "_id";
    private static final int COLONNE_ID_etudiant_ID = 0;

    private static final String COLONNE_ID_note = "_id";
    private static final int COLONNE_ID_note_ID = 0;

    private static final String COLONNE_note = "note";
    private static final int COLONNE_note_ID = 1;

    private static final String COLONNE_note_idEtudiant = "id_etudiant";
    private static final int COLONNE_note_idEtudiant_ID = 2;

    private static final String COLONNE_NAME = "nom";
    private static final int COLONNE_NAME_ID = 2;

    private static final String COLONNE_MATRICULE = "matricule";
    private static final int COLONNE_MATRICULE_ID = 1;



    // L�instance de la base qui sera manipulee au travers de cette classe.
    private SQLiteDatabase baseDonnees;
    private EtudiantDbHelper  baseHelper;
    public EtudiantAdapter(Context ctx) {
        baseHelper = new EtudiantDbHelper(ctx, BASE_NOM, null, BASE_VERSION);
    }
    /**
     * Ouvre la base de donnees en ecriture.
     */
    public SQLiteDatabase open() {
        baseDonnees = baseHelper.getWritableDatabase();
        return baseDonnees;
    }
    /**
     * Ferme la base de donnees.
     */
    public void close() {
        baseDonnees.close();
    }
    public SQLiteDatabase getBaseDonnees() {
        return baseDonnees;
    }
    /**
     * Recup�re un tupple en fonction de son id.
     */
    public Etudiant getEtudiantByID(int id) {
        Cursor c = baseDonnees.query(TABLE_NAME_etudiant, new String[] {
                        COLONNE_ID_etudiant, COLONNE_MATRICULE, COLONNE_NAME }, null, null, null,
                COLONNE_ID_etudiant + " = " + id, null);
        return cursorToEtudiant(c);
    }

    public Note getNoteByIDetudiant(int idEtu) {
        Cursor c = baseDonnees.query(TABLE_NAME_note, new String[] {
                        COLONNE_ID_note, COLONNE_note, COLONNE_note_idEtudiant }, null, null, null,
                COLONNE_note_idEtudiant + " = " + idEtu, null);
        return cursorToNote(c);
    }
    /**
     * Retourne toutes les Etudiants de la base de donnees.
     */
    public ArrayList<Etudiant> getAllEtudiants() {
        Cursor c = baseDonnees.query(TABLE_NAME_etudiant, new String[]{
                COLONNE_ID_etudiant, COLONNE_MATRICULE, COLONNE_NAME}, null, null, null, null, null);
        return cursorToEtudiants(c);
    }
    /**
     * Ins�re une plan�te dans la table des plan�tes.
     */
    public long insertEtudiant(Etudiant etudiant) {
        ContentValues valeurs = new ContentValues();
        valeurs.put(COLONNE_MATRICULE, etudiant.getMatricule());
        valeurs.put(COLONNE_NAME, etudiant.getNom());
        return baseDonnees.insert(TABLE_NAME_etudiant, null, valeurs);
    }

    public long insertEtudiant(ContentValues valeurs) {
        return baseDonnees.insert(TABLE_NAME_etudiant, null, valeurs);
    }


    /**
     * Met � jour une Etudiant dans la table des plan�tes.
     */
   /* public int updateDataEnFr(int id, DataEnFr dataEnFrToUpdate) {
        ContentValues valeurs = new ContentValues();
        valeurs.put(COLONNE_WORD, dataEnFrToUpdate.getWord());
        valeurs.put(COLONNE_WORDFR, dataEnFrToUpdate.getWordFr());
        valeurs.put(COLONNE_SENTENCE, dataEnFrToUpdate.getSentence());
        valeurs.put(COLONNE_PRINT, dataEnFrToUpdate.getPrint());
        return baseDonnees.update(TABLE_DATAENFR, valeurs, COLONNE_ID + " = "
                + id, null);
    }

    public int updateDataEnFr(ContentValues valeurs, String where,
                              String[] whereArgs) {
        return baseDonnees.update(TABLE_DATAENFR, valeurs, where, whereArgs);
    }
*/
    /**
     * Supprime une DataEnFr � partir de son word.
     */
  /*  public int removePlanete(String word) {
        return baseDonnees.delete(TABLE_DATAENFR, COLONNE_WORD + " LIKE "
                + word, null);
    }*/
    /**
     * Supprime une DataEnFr � partir de son id.
     */
   /* public int removePlanete(int id) {
        return baseDonnees.delete(TABLE_DATAENFR, COLONNE_ID + " = " + id,
                null);
    }

    public int removeDataEnFr(String where, String[] whereArgs) {
        return baseDonnees.delete(TABLE_DATAENFR, where, whereArgs);
    }*/
    /* @param c
        * Le curseur � utiliser pour recuperer les donnees de la DataEnFr.
        * @return Une instance d�une DataEnFr avec les valeurs du curseur.
        */
    private Etudiant cursorToEtudiant(Cursor c)
    {
        // Si la requ�te ne renvoie pas de resultat.
        if (c.getCount()== 0)
            return null;
        Etudiant retEtu = new Etudiant();
        // Extraction des valeurs depuis le curseur.
        retEtu.setId(c.getInt(COLONNE_ID_etudiant_ID));
        retEtu.setMatricule(c.getString(COLONNE_MATRICULE_ID));
        retEtu.setNom(c.getString(COLONNE_NAME_ID));

        // Ferme le curseur pour liberer les ressources.
        c.close();
        return retEtu;
    }

    private Note cursorToNote(Cursor c)
    {
        // Si la requ�te ne renvoie pas de resultat.
        if (c.getCount()== 0)
            return null;
        Note retNote = new Note();
        // Extraction des valeurs depuis le curseur.
        retNote.setId(c.getInt(COLONNE_note_ID));
        retNote.setNote(c.getInt(COLONNE_note_ID));
        retNote.setIdEtudiant(c.getInt(COLONNE_ID_etudiant_ID));

        // Ferme le curseur pour liberer les ressources.
        c.close();
        return retNote;
    }

    private ArrayList<Etudiant> cursorToEtudiants(Cursor c) {
        // Si la requ�te ne renvoie pas de resultat.
        if (c.getCount() == 0)
            return new ArrayList<Etudiant>(0);
        ArrayList<Etudiant> retEtus = new ArrayList<Etudiant>(c.getCount());
        c.moveToFirst();
        do {
            Etudiant etu = new Etudiant();

           // etu = cursorToEtudiant(c);

            etu.setId(c.getInt(COLONNE_ID_etudiant_ID));
            etu.setMatricule(c.getString(COLONNE_MATRICULE_ID));
            etu.setNom(c.getString(COLONNE_NAME_ID));

            retEtus.add(etu);
        } while (c.moveToNext());
        // Ferme le curseur pour liberer les ressources.
        c.close();
        return retEtus;
    }

    private ArrayList<Note> cursorToNotes(Cursor c) {
        // Si la requ�te ne renvoie pas de resultat.
        if (c.getCount() == 0)
            return new ArrayList<Note>(0);
        ArrayList<Note> retNotes = new ArrayList<Note>(c.getCount());
        c.moveToFirst();
        do {
            Note n = new Note();
           // n = cursorToNote(c);
            n.setId(c.getInt(COLONNE_note_ID));
            n.setNote(c.getInt(COLONNE_note_ID));
            n.setIdEtudiant(c.getInt(COLONNE_ID_etudiant_ID));

            retNotes.add(n);
        } while (c.moveToNext());
        // Ferme le curseur pour liberer les ressources.
        c.close();
        return retNotes;
    }
}
