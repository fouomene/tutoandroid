package arielle.testarielle.dao;

/**
 * Created by FOUOMENE on 20/06/2015.
 */
public class Note {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    public int getIdEtudiant() {
        return idEtudiant;
    }

    public void setIdEtudiant(int idEtudiant) {
        this.idEtudiant = idEtudiant;
    }

    private int id;
    private int note;
    private int idEtudiant;
}
