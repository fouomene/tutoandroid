package arielle.testarielle.dao;

/**
 * Created by FOUOMENE on 20/06/2015.
 */
public class Etudiant {

    private int id;
    private String matricule;
    private String nom;

    public void setId(int id) {
        this.id = id;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getId() {
        return id;
    }

    public String getMatricule() {
        return matricule;
    }

    public String getNom() {
        return nom;
    }


}
