package arielle.testarielle.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by FOUOMENE on 20/06/2015.
 */
public class EtudiantDbHelper extends SQLiteOpenHelper {

    private static final String TABLE_NAME_etudiant = "etudiant";
    private static final String TABLE_NAME_note = "note";
    private static final String COLONNE_ID_etudiant = "_id";
    private static final String COLONNE_ID_note = "_id";
    private static final String COLONNE_note = "note";
    private static final String COLONNE_note_idEtudiant = "id_etudiant";
    private static final String COLONNE_NAME = "nom";
    private static final String COLONNE_MATRICULE = "matricule";

    // La requ�te de cr�ation de la structure de la base de donn�es.
    private static final String REQUETE_CREATION_etudiant = "create table "
            + TABLE_NAME_etudiant + " (" + COLONNE_ID_etudiant
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLONNE_MATRICULE + " text not null, "
            + COLONNE_NAME + " text not null); ";

    private static final String REQUETE_CREATION_note = "create table "
            + TABLE_NAME_note + " (" + COLONNE_ID_note
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLONNE_note
            + " integer not null, " + COLONNE_note_idEtudiant + " integer not null,"
            // Set up the yearmonth column as a foreign key to yearmonth table.
            +" FOREIGN KEY (" + COLONNE_ID_etudiant + ") REFERENCES " +
            TABLE_NAME_etudiant + " (" + COLONNE_ID_etudiant + ") );";

     public EtudiantDbHelper(Context context, String nom,
                              SQLiteDatabase.CursorFactory cursorfactory, int version) {
        super(context, nom, cursorfactory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(REQUETE_CREATION_etudiant);
        db.execSQL(REQUETE_CREATION_note);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Dans notre cas, nous supprimons la base et les donn�es pour en cr�er
        // une nouvelle ensuite. Vous pouvez cr�er une logique de mise � jour
        // propre � votre base permettant de garder les donn�es � la place.
        db.execSQL("DROP TABLE " + TABLE_NAME_note + ";");
        db.execSQL("DROP TABLE " + TABLE_NAME_etudiant + ";");
        // Cr�ation de la nouvelle structure.
        onCreate(db);
    }

}
