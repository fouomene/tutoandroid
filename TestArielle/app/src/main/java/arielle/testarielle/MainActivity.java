package arielle.testarielle;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import arielle.testarielle.dao.Etudiant;
import arielle.testarielle.dao.EtudiantAdapter;


public class MainActivity extends  ActionBarActivity {

    private EtudiantAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //insertion des donn�es dasn la base de donn�e
        adapter= new  EtudiantAdapter(getBaseContext());

        adapter.open();

        // Get a reference to the button,
        ((Button)findViewById(R.id.buttonOk)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Etudiant e = new Etudiant();
                e.setNom(((TextView) findViewById(R.id.editTextNom)).getText().toString());
                e.setMatricule(((TextView) findViewById(R.id.editTextMatricule)).getText().toString());

                int idEtudiant = Integer.parseInt("" + adapter.insertEtudiant(e));

                Intent intent = new Intent(getBaseContext(), DetailActivity.class);

                intent.putExtra("idEtudiant", String.valueOf(idEtudiant));

                startActivity(intent);


            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.close();
    }
}
