package arielle.testarielle;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import arielle.testarielle.dao.EtudiantAdapter;


/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    private final String LOG_TAG = DetailActivityFragment.class.getSimpleName();

    private DetailsAdapter mEtudiantAdapter;

    private EtudiantAdapter adapter;


    private ListView mListView;


    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        //insertion des donnees dasn la base de donnee
        adapter= new EtudiantAdapter(getActivity());
        adapter.open();

        // The ArtistAdapter will take data from a source and
        // use it to populate the ListView it's attached to.
        mEtudiantAdapter = new DetailsAdapter(getActivity());
        // Get a reference to the ListView, and attach this adapter to it.
        mListView = (ListView) rootView.findViewById(R.id.listview_etudiant);
        mListView.setAdapter(mEtudiantAdapter);


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        mEtudiantAdapter.updateEtudiants(adapter.getAllEtudiants());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter.close();
    }
}
