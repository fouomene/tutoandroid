package arielle.testarielle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import arielle.testarielle.dao.Etudiant;

/**
 * Created by FOUOMENE on 20/06/2015.
 */
public class DetailsAdapter extends BaseAdapter {

    private final String LOG_TAG = DetailsAdapter.class.getSimpleName();

    private List<Etudiant> mEtudiants = Collections.emptyList();
    private final Context context;

    private static class ViewHolder {
        public final TextView mNom;
        public final TextView mMatricule;
        public final TextView mNote;

        public ViewHolder(TextView nom,TextView matricule, TextView note ) {
            mNom = nom;
            mMatricule= matricule;
            mNote = note;

        }
    }

    // the context is needed to inflate views in getView()
    public DetailsAdapter(Context context) {
        this.context = context;
    }

    public void updateEtudiants(List<Etudiant> etudiants) {

            this.mEtudiants = etudiants;

            notifyDataSetChanged();


    }


    @Override
    public int getCount() {
        return mEtudiants.size();
    }

    // getItem(int) in Adapter returns Object but we can override
    // it to Artist thanks to Java return type covariance
    @Override
    public Etudiant getItem(int position) {
        return mEtudiants.get(position);
    }

    // getItemId() is often useless, I think this should be the default
    // implementation in BaseAdapter
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        TextView nom;
        TextView matricule;
        TextView note;
        if (convertView == null) {
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.liste_item_etudiant, parent, false);

            nom = (TextView)convertView.findViewById(R.id.textViewNom);
            matricule = (TextView)convertView.findViewById(R.id.textViewMatricule);
            note = (TextView)convertView.findViewById(R.id.textViewNote);

             convertView.setTag(new ViewHolder(nom, matricule, note));
        } else {
            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            nom = (TextView)convertView.findViewById(R.id.textViewNom);
            matricule = (TextView)convertView.findViewById(R.id.textViewMatricule);
            note = (TextView)convertView.findViewById(R.id.textViewNote);

        }

            Etudiant etudiant = getItem(position);

            nom.setText(etudiant.getNom());
            matricule.setText(etudiant.getMatricule());
            note.setText("15");


        return convertView;

    }

}
